// console.log("Hello world");

// 1.
	let trainer = {
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
		friends: {
			hoenn: ["May", "Max"],
			kanto: ["Brock", "Misty"]
		},
		talk: function(){
				console.log(trainer.pokemon[0] + "! I choose you!");
			}
	}

	console.log(trainer);

	console.log("Result of dot notation:")
	console.log(trainer.name);

	console.log("Result of square bracket notation:")
	console.log(trainer["pokemon"]);

	console.log("Result of talk method:")
	trainer.talk();

// 2.
	function Pokemon(name, level) {
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		// Method
		this.tackle = function(targetPokemon) {
			console.log(this.pokemonName + " tackled " + targetPokemon.pokemonName);
			let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack;
			console.log(targetPokemon.pokemonName + "'s health is now reduced to " + hpAfterTackle);
			targetPokemon.pokemonHealth = hpAfterTackle;
			if(targetPokemon.pokemonHealth <= 0){
				targetPokemon.faint();
			}
			console.log(targetPokemon);
		}

		this.faint = function() {
			console.log(this.pokemonName + " fainted")
		}
	}

	let pikachu = new Pokemon("Pikachu", 12);
	let geodude = new Pokemon("Geodude", 8);
	let mewtwo = new Pokemon("Mewtwo", 100);

	console.log(pikachu);
	console.log(geodude);
	console.log(mewtwo);

// 3.
	geodude.tackle(pikachu);
	mewtwo.tackle(geodude);